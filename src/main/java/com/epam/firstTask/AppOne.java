package com.epam.firstTask;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class AppOne {
    public static Logger logger= LogManager.getLogger(AppOne.class);
    public static void main(String[] args) {
        Printable max = (a,b,c)-> a>b && a>c ?a:(b>a && b>c) ?b:c;
        //System.out.println("Max: "+max.method(10,34,6));
        Printable avg = (a,b,c) -> (a+b+c)/3;
        //System.out.println("Avarage: "+avg.method(10,3,2));
        logger.info("Max: "+max.method(10,34,6)+"\n");
        logger.info("Avarage: "+avg.method(10,3,2)+"\n");
    }
}
