package com.epam.secondTask;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * <h1>The task2</h1>
 * <p>The AppTwo task implement 4 commands with next ways:
 * command as lambda function, as method reference,
 * as anonymous class, as object of command class. </p>
 *
 * @author Daria Ferbey
 * @version 1.1
 * @since 2020-04-12
 */
public class AppTwo {
    private static Scanner input = new Scanner(System.in);
    private static List<Command> list = new ArrayList<>();

    /**
     * This method is used to get name
     * command for reference method.
     *
     * @param name name of command
     */
    private static String getStr(String name) {
        return "Command as " + name + " method";
    }

    /**
     * This method is used to get name
     * command for object.
     *
     * @param name name of command
     */
    private String getString(String name) {
        return "Command as " + name;
    }

    /**
     * @param args unused
     */
    public static void main(String[] args) {
        list.add(new Command() {
            @Override
            public String getName(String name) {
                return "Command as " + name + " class";
            }
        });
        list.add(str -> "Command as lambda expression");
        list.add(AppTwo::getStr);
        list.add(new AppTwo()::getString);

        System.out.println("Menu:");
        System.out.println("1.Command as anonymous class");
        System.out.println("2.Command as lambda expression");
        System.out.println("3.Command as reference method");
        System.out.println("4.Command as object");
        System.out.println();

        boolean match = false;
        while (match == false) {
            System.out.print("Enter name(anonymous/lambda/reference/object): ");
            String name = input.nextLine();

            switch (name) {
                case "anonymous":
                    match = true;
                    System.out.print(list.get(0).getName(name));
                    break;
                case "lambda":
                    match = true;
                    System.out.print(list.get(1).getName(name));
                    break;
                case "reference":
                    match = true;
                    System.out.print(list.get(2).getName(name));
                    break;
                case "object":
                    match = true;
                    System.out.print(list.get(3).getName(name));
                    break;
                default:
                    match = false;
                    System.out.println("Try again!");
            }

        }

    }
}
