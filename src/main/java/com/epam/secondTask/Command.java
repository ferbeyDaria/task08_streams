package com.epam.secondTask;


public interface Command {
    String getName(String name);
}
