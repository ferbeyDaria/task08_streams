package com.epam.fourthTask;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.stream.Collectors;


public class AppFour {
    public static Logger logger = LogManager.getLogger(AppFour.class);

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter text: ");
        List<String> arrayText = new ArrayList<>();
        String line = null;
        while (!((line = scanner.nextLine()).isEmpty())) {
            System.out.print("Enter text: ");
            String text = line;
            arrayText.add(text);
        }
        System.out.println(arrayText);

        //count number of unique words
        long uniqueWordCount = arrayText.stream()
                .map(String::toLowerCase)
                .collect(Collectors.groupingBy(w -> w, Collectors.counting()))
                .entrySet().stream()
                .filter(e -> e.getValue() == 1)
                .count();


        //word count
        System.out.println("Word count");
        Map<String, Long> wordCount = arrayText
                .stream()
                .collect(Collectors.groupingBy(w -> w, Collectors.counting()));
        for (Map.Entry<String, Long> item : wordCount.entrySet()) {

            System.out.println(item.getKey() + " - " + item.getValue());
        }

        //sorted of unique word
        List<String> sort = new ArrayList<>();
        for (Map.Entry<String, Long> item : wordCount.entrySet()) {
            sort = arrayText.stream()
                    .map(String::toLowerCase)
                    .sorted()
                    .collect(Collectors.toList());
        }


        System.out.println("Number of unique: " + uniqueWordCount);
        System.out.println(sort);

    }
}
