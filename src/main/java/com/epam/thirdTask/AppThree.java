package com.epam.thirdTask;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AppThree {
    public static Logger logger = LogManager.getLogger(AppThree.class);
    List<Integer> list = new ArrayList<>();
    Random random = new Random();

    public List<Integer> listOfRandomNumber() {
        list = random
                .ints(8)
                .boxed()
                .collect(Collectors.toList());
        return list;
    }

    public List<Integer> listOfNumber() {
        list = Stream
                .generate(random::nextInt)
                .limit(8).collect(Collectors.toList());
        return list;
    }

    public static void main(String[] args) {
        List<Integer> list = new AppThree().listOfNumber();
        //System.out.println(list);
        logger.info(list);

        //count sum using sum Stream method
        Stream<Integer> stream = list.stream();
        long sum = stream
                .mapToInt(i -> i)
                .sum();
        //System.out.println("Sum = " + sum);
        logger.info("Sum = ",sum);

        //count sum using reduce method
        Stream<Integer> streamSum = list.stream();
        Optional<Integer> sumReduce = streamSum
                .reduce((left,right)->left+right);
        //System.out.print("Sum = ");
        //sumReduce.ifPresent(System.out::println);

        //count average, min, max, sum of list values.
        IntSummaryStatistics streamed;
        streamed = list
                .stream()
                .mapToInt(x -> x)
                .summaryStatistics();
       // System.out.println(streamed);
        logger.info(streamed);

        //count number of values that are bigger than average
        double avg = streamed.getAverage();
        Stream<Integer> streamInt = list.stream();
        long count = streamInt
                .filter(i -> i > avg)
                .count();
        //System.out.println("Count number of values that are bigger than average: " + count);
        logger.info("Count number of values that are bigger than average: " + count);

    }
}
